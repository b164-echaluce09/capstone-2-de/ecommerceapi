const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")

const bcrypt = require("bcrypt")


const auth = require("../auth")



registerUser = (requestBody) => {

	
	let newUser = new User ({
			firstName: requestBody.firstName,
			lastName: requestBody.lastName,
			age: requestBody.age,
			gender: requestBody.gender,
			email: requestBody.email,
			password: bcrypt.hashSync(requestBody.password,10),
			mobileNo: requestBody.mobileNo,
			deliveryAddress: requestBody.deliveryAddress
		})

		return newUser.save().then((result,error) => {
			if(error){
				return false
			}
			else {
				return true
			}
		})
	
	
}

checkEmailExists = (reqBody) =>{
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		} else{
			//no duplicate email found
			return false;
		}
	})
}

getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};


loginUser = async (requestBody) => {

	let message = "User login"
	let isUserExists = await User.find({ email : requestBody.email}).then((result,error) => {
		
		if (result.length > 0){
			return	true
		}
		else {
			
			return false
		}
	})

	if (isUserExists) {
		message = await User.findOne({ email : requestBody.email}).then((result,error) => {
			const comparePassword = bcrypt.compareSync(requestBody.password,result.password)
				if (comparePassword) {
					return {accessToken:auth.createAccessToken(result.toObject())}
				}
				else {
					//"Password does not match"
					return false
				}
		})
	}
	else {
		message = "User does not exist"
	}
	return message
}


changePassword = (userId, requestBody) => {
	return User.findById(userId).then((result,error) => {
		const comparePassword = bcrypt.compareSync(requestBody.currentPassword,result.password)
		if(error) {
			return false
		}
		else if (comparePassword) {
			result.password = bcrypt.hashSync(requestBody.newPassword,10)

			return result.save().then((result,error) => {
				if(error) {
					return false
				}
				else {
					return true
				}
			})
		}
		else {
			return false
		}
	})
}

updateInfo = (userId, requestBody) => {
	return User.findById(userId).then((result,error) => {
		const comparePassword = bcrypt.compareSync(requestBody.password,result.password)
		if(error) {
			return false
		}
		else if (comparePassword) {

			result.firstName= requestBody.firstName,
			result.lastName= requestBody.lastName,
			result.age= requestBody.age,
			result.gender= requestBody.gender,
			result.mobileNo= requestBody.mobileNo,
			result.deliveryAddress= requestBody.deliveryAddress

			return result.save().then((result,error) => {
				if(error) {
					return false
				}
				else {
					return true
				}
			})
		}
		else {
			return false
		}
		
	})
}

setAdmin = (requestParams, isAdmin) => {
	return User.findById(requestParams).then((result,error) => {
		if(error) {
			return false
		}
		else if (isAdmin){
				// console.log(result)
				// return `Successfully updated admin status for `

				result.isAdmin = true

				return result.save().then((result,error) => {
					if(error) {
						return false
					}
					else {
						return `Successfully updated admin status for ${result.email}`
					}
				})

			}//end
			
		else if (!isAdmin) {
				return "User is unauthorized to update admin status."
			}

		else  {
				return "Please check userId"
			}	


	}).catch(error => {
					console.log(typeof error)
					console.warn(error.message) 
					return "Please check userId"
				})

}


updateQty = (userId, cart) => {
	return User.findById(userId).then((result,error) => {
		if(error) {
			return false
		}
		else {
				// console.log(result)
				// return `Successfully updated admin status for `

				result.cart[cart.index].productQty = cart.productQty

				return result.save().then((result,error) => {
					if(error) {
						return false
					}
					else {
						return true
					}
				})
			}//end
	})

}

removeItem = (userId, index) => {
	return User.findById(userId).then(user => {

		user.cart.splice(index,1)

		return user.save().then((result,error) => {
			if(error) {
				return false
			}
			else {
				return true
			}
		})
	})

}

addToCart = async (data) => {

	let product = await Product.findById(data.productId).then((productData,error) => {
						if (error){
							return false
						}
						else {
							return productData
						}
					})

	if (product.productPrice  > 0) {
		return User.findById(data.userId).then(user => {

				user.cart.push({ 
					productId : data.productId, 
					productName : product.productName, 
					productCategory : product.productCategory, 
					productVolume : product.productVolume, 
					productPrice : product.productPrice, 
					productQty : data.productQty } )

				return user.save().then((result,error) => {
					if(error) {
						return false
					}
					else {
						return true
					}
				})
			})
 
	}
	

	
}



// ******************************
checkout = async (userIdfromRoute) => {
			let price = 0;
			let purchase = [];
			let qty = [];
			let pPrice = [];
			let i = 0;
			let paidCounter = 0;
			let cartItems = 0;
			let createdOrder = 0;

	let stepOne = await User.findById(userIdfromRoute).then((userProfile,error) => {
		if (error) {
			return false
		}
		else if (userProfile.cart.length == 0) {
			// nothing in cart
			// return "Cart is empty"
			return false
		}
		else {
			// cart is NOT EMPTY
			// console.log(`cart content: ${userProfile.cart.length}`)

			// store the accumulated value of the prices 
			// store the list of product in the cart

			cartItems = userProfile.cart.length

			for(i = 0; i < cartItems; i++ ){

				
				if (userProfile.cart[i].isCheckedOut) {
					// console.log("will not do anything since checkout is true")
					paidCounter ++
					continue
				}
				else {
					
					price = price + (userProfile.cart[i].productQty * userProfile.cart[i].productPrice)
					purchase.push(userProfile.cart[i].productId)
					qty.push(userProfile.cart[i].productQty)
					pPrice.push(userProfile.cart[i].productPrice)


									
				}//end else

			} //end for

			console.log(`cartItems: ${i}`)
			console.log(`already paid: ${paidCounter}`)

			if (paidCounter == cartItems){
				// return "All Items are already paid."
				return false
			}

			else {

				console.log(`purchased items: ${purchase.length}`)
				console.log(`totalAmount: ${price}`)

				let newOrder = new Order({
					userId:userIdfromRoute,
					productId: purchase,
					productQty: qty,
					productPrice: pPrice,
					totalAmount: price,
				})


				return newOrder.save().then((savedOrder,error) => {
					if(error) {
						return false
					}
					else {
						createdOrder = savedOrder.id
						
						console.log(`Order Saved! id: ${createdOrder}`)


						return true
					}
				})

			}

			// return true
		}

	}) //find user






let stepTwo = await User.findById(userIdfromRoute).then((userProfile,error) => {
			if (error) {
				return false
			}
			else {
				for(i = 0; i < cartItems; i++ ){
					if (userProfile.cart[i].isCheckedOut) {
						// console.log("will not do anything since checkout is true")
						continue
					}
					else { //**

						// userProfile.cart[i].orderId = createdOrder
						userProfile.cart[i].isCheckedOut = true 
						userProfile.save((result,error) => {
							if(error) {
								return false
							}
							// else {
							// 	console.log(`Successfully updated user checkout`)
							// 	// return true
							// }
						})

						userProfile.cart[i].orderId = createdOrder
						userProfile.save((result,error) => {
							if(error) {
								return false
							}
							// else {
							// 	console.log(`Successfully updated user orderId`)
							// 	// return true
							// }
						})
					} //**
				}
			}
	})//return



	if(stepOne && stepTwo){
		// console.log(`step ${stepOne} ${stepTwo}`)
		return true
	}
	else {
		// console.log(`step ${stepOne} ${stepTwo}`)
		return true
	}


}


module.exports = {
	registerUser,
	loginUser,
	setAdmin,
	addToCart,
	checkout,
	checkEmailExists,
	getProfile,
	updateQty,
	removeItem,
	changePassword,
	updateInfo
}