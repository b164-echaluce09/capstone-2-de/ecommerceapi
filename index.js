// set up dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
require('dotenv').config()

// Route Connection
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")

// end of Route connection


// server setup
const app = express()

// all all resources to access backend
// enable all cors request
app.use(cors())



app.use(express.json())
app.use(express.urlencoded({extended :true}))



// set route endpoint
app.use("/api/users", userRoutes)
app.use("/api/products", productRoutes)
app.use("/api/orders", orderRoutes)

// end of route endpoint





// mongoDB Connection
//Connect to our MongoDB connection
mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//MongoDB connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`)
})



