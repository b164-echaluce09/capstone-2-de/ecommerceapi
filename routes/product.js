const express = require("express")
const router = express.Router()

const productController = require("../controllers/product")

const auth = require("../auth")


// add product to database ADMIN ONLY
router.post("/create", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if (userData.isAdmin) {
		productController.addProduct(request.body).then(result => response.send(result))
	}
	else {
		response.send("User is unauthorized to add product.")
	}
})

// get all  products
router.get("/allProducts", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if (userData.isAdmin) {
		productController.allProduct().then(result => response.send(result))
	}
	else {
		response.send("Please use admin account.")
	}
})

// get all active products
router.get("/active", (request,response) => {
	productController.activeProduct().then(result => response.send(result))
})


// retrieve single product
router.get("/:id",(request,response) => {
	productController.getProduct(request.params.id).then(result => response.send(result))
})


// update product ADMIN only
router.put("/:id/update", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if (userData.isAdmin) {
		productController.updateProduct(request.params.id,request.body).then(result => response.send(result))
	}
	else {
		response.send("User is unauthorized to update product.")
	}
})



// archive product ADMIN ONLY
router.put("/:id/archive", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)

	if (userData.isAdmin) {
		productController.archiveProduct(request.params.id).then(result => response.send(result))
	}
	else {
		return response.send("Please use admin account.")
	}

})

// activate product ADMIN ONLY
router.put("/:id/activate", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)

	if (userData.isAdmin) {
		productController.activateProduct(request.params.id).then(result => response.send(result))
	}
	else {
		return response.send("Please use admin account.")
	}

})



module.exports = router