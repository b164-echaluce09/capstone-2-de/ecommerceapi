const express = require("express")
const router = express.Router()

const orderController = require("../controllers/order")

const auth = require("../auth")


// all orders
router.get("/allOrders", auth.verify, (request,response) => {
	let userData = auth.decode(request.headers.authorization)

	if (userData.isAdmin){
		orderController.allOrders().then(result => response.send(result))
	}
	else {
		response.send("Please use an admin account")
	}
})



// user orders
router.get("/userOrders", auth.verify, (request,response) => {
	let userData = auth.decode(request.headers.authorization)

	if (!userData.isAdmin){
		orderController.userOrders(userData.id).then(result => response.send(result))
	}
	else {
		response.send("Please use a non admin account")
	}
})



//processOrder
router.put("/processOrder", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	
	
	if (userData.isAdmin){
		orderController.processOrder(request.body).then(result => response.send(result))
	}
})


// orders details
router.get("/:orderId", auth.verify, (request,response) => {
	let userData = auth.decode(request.headers.authorization)

	if (userData.isAdmin){
		orderController.orderDetails(request.params.orderId).then(result => response.send(result))
	}
	else {
		response.send("Please use an admin account")
	}
})




module.exports = router;