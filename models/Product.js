const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({

  isActive: {
    type: Boolean,
    default: true
  },
  createdOn:{
  	type: Date,
  	default: new Date()
  },
  productName: {
		type:String,
		required:[true,"Product name is required"]
	},
  productCategory:{
		type:String,
		required:[true,"Product category is required"]
	},
  productVolume:{
		type:String,
		required:[true,"Product volume required"]
	},
  productDescription: {
		type:String,
		required:[true,"Product description is required"]
	},
  productPrice:{
		type:Number,
		required:[true,"Price is required"]
	}


})



module.exports = mongoose.model("Product",productSchema)
