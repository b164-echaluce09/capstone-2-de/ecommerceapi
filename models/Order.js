const mongoose = require("mongoose")

const newOrder = new mongoose.Schema({
	userId:{
    	type:String,
   		required:[true,"User ID is required."]
 		},
	productId: [{
    	type:String,
    	required:[true,"Product ID is required."]
 		}],

 	productQty: [{
    	type:String,
    	required:[true,"Product ID is required."]
 		}],
 		
 	productPrice: [{
    	type:String,
    	required:[true,"Product ID is required."]
 		}],
	totalAmount: {
    	type:Number,
    	required:[true,"Total amount is required."]
 		},
	isPaid: {
    	type:Boolean,
    	default: true
 		},
	purchasedOn: {
    	type:Date,
    	default: new Date()
 		},
	deliveryStatus:{
    	type:String,
    	default: "Pending"
 		},
 	isProcessed: {
          type:Boolean,
          default: false
        }	

})

module.exports = mongoose.model("Order", newOrder)