=======USERS=======

{
  _id: objectID,
  isAdmin:Boolean,
  firstName: String,
  lastName:String,
  age:String,
  gender:String,
  email:String,
  password:String,
  mobileNo:String,
  deliveryAddress:String
  cart: [
      {
        productID:string,
        orderID:string,
        isCheckedOut: Boolean
      }
    ]
}



========PRODUCT========

{
  _id: objectID,
  isActive: Boolean,
  createdOn:Date,
  productName: String,
  productCategory:String,
  productVolume:String,
  productDescription: String,
  productPrice: Number,
}


======ORDER======
{
  _id: objectID,
  userID:String,
  productID: [String],
  isPaid: Boolean,
  totalAmount: Number,
  purchasedOn: Date,
  deliveryStatus:String,
}




